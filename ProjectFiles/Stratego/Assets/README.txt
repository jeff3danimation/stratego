Games are generally made in a structure of MUD development, though not always.

MUD (also known as Multi-User Dungeons) came out in the 1980's.
MUD's were the first multi-online text roleplaying games that allowed incredulous
amounts of people to play on the same server. Graphics were limited to letters,
numbers, symbols, etc.(imagine a game made of this kind of art)

  .-'---`-.
,'          `.
|             \
|              \
\           _  \
,\  _    ,'-,/-)\
( * \ \,' ,' ,'-)
 `._,)     -',-')
   \/         ''/
    )        / /
   /       ,'-'



MUD game development structure is based on the following:
 - AI
 - Player
 - Levels / Environments
 - Code / Scripts

Now, as rigid as that might seem, it's not. Those are the top most parent directories,
everything else would get segmented below that. We probably won't be doing this, entirely,
however, this information is really good in terms of project development. We want to make
sure that we have the ability to follow a structure so assets are not just being thrown
about everywhere (thusly making the project dirty and hard to navigate).

For now, I've created a project structure with the following directories:
 - Art
 - Scenes
 - Scripts
 - Sounds

To break this down:
 - Art: Contains all visual information (something MUD's didn't have, in terms of non-ascii gfx)
 - Scenes: Environments / Levels
 - Scripts: Contains the AI / Player / etc.
 - Sounds: Sound information that MUD games did not often have, as not all
   games had been coded with 8-bit soundtracks.

Scripts should contain everything regarding the player, including their movement,
attack, defend, power-up, etc., AI, camera movement and parents, etc.

Art should contain everything regarding to visuals, sfx, vfx, UI/UX, materials, etc.

Scenes should *ONLY* contain levels.

Sounds should *ONLY* contain music and sfx.


Update

Testing