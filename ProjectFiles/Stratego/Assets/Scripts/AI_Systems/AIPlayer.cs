using System.Collections.Generic;
using UnityEngine;

public class AIPlayer
{
    public AIPlayer()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }
    GameManager gameManager;

    virtual public void DoAI()
    {
        Dictionary <int, List<Vector2Int>> localOutput = new Dictionary <int, List<Vector2Int>>();
        localOutput = MakeDictionaryOfLegalMoves();
        ChooseBoardLocationAndMove(localOutput);
    }

    virtual protected Dictionary<int, List<Vector2Int>> MakeDictionaryOfLegalMoves()
    {
        // Chooses a valid character piece to move
        // public GameManager gm = GameManager.FindObjectOfType<GameManager>();
        Dictionary<int, List<Vector2Int>> possibleMoves = new Dictionary<int, List<Vector2Int>>();

        List<GameObject> allPieces = gameManager.currentPlayer.pieces;
        foreach (GameObject piece in allPieces)
        {
            if (piece != null && gameManager.MovesForPiece(piece).Count > 0)
            {
                List<Vector2Int> legalMoves = gameManager.MovesForPiece(piece);
                possibleMoves.Add(piece.GetComponent<Piece>().internalIdNumber, legalMoves);
                return possibleMoves;
            }
        }
        return null;
    }

    virtual protected void ChooseBoardLocationAndMove(Dictionary<int, List<Vector2Int>> possibleMoves)  // GameObject piece)
    {
        // Chooses a valid character and board location for chosen character piece

        if (possibleMoves != null)
        {
            int rand = Random.Range(0, possibleMoves.Keys.Count);
            List<int> allAiPieces = new List<int>(possibleMoves.Keys);
            List<GameObject> allPieces = gameManager.currentPlayer.pieces;
            GameObject characterpiece;

            foreach (GameObject piece in allPieces)
            {
                if (piece != null)
                {
                    if (piece.GetComponent<Piece>().internalIdNumber == allAiPieces[rand])
                    {
                        characterpiece = piece;
                        // Get random location
                        List<Vector2Int> locations = possibleMoves[allAiPieces[rand]];
                        int randloc = Random.Range(0, locations.Count);

                        // Move the piece
                        gameManager.Move(characterpiece, locations[randloc]);
                    }
                    else
                    {
                        // Wait, because AI might not have found something yet... Don't freak out... Eventually we need to check if there are no more valid moves
                        // TODO: Check for no more moves.
                    }
                }
            }
        }
    }
}