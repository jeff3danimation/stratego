﻿using UnityEngine;

public class Board : MonoBehaviour
{
    public Material selectedMaterial;
    public Material TEMPORARY;

    public GameObject AddPiece(GameObject piece, int col, int row)
    {
        Vector2Int gridPoint = Geometry.GridPoint(col, row);
        GameObject newPiece = Instantiate(piece, Geometry.PointFromGrid(gridPoint), Quaternion.identity, gameObject.transform);
        return newPiece;
    }

    public void RemovePiece(GameObject piece)
    {
        if (piece != null)
        {
            Destroy(piece);
        }
    }

    public void MovePiece(GameObject piece, Vector2Int gridPoint)
    {
        if (piece != null)
        {
            piece.transform.position = Geometry.PointFromGrid(gridPoint);
        }
    }

    public void SelectPiece(GameObject piece)
    {
        if (piece != null)
        {
            MeshRenderer renderers = piece.GetComponentInChildren<MeshRenderer>();
            TEMPORARY = renderers.material;
            renderers.material = selectedMaterial;
        }
    }

    public void DeselectPiece(GameObject piece)
    {
        if (piece != null)
        {
            MeshRenderer renderers = piece.GetComponentInChildren<MeshRenderer>();
            renderers.material = TEMPORARY;
        }
    }
}
