﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool EscapeMenuOff = true;

    public bool hotseat;
    public Camera cam;

    // Get our board
    public Board board;

    // Get escape menu
    public GameObject EscapeMenu;

    // Random
    private static Random rng = new Random();

    // Get red player pieces
    public GameObject redMarshall;
    public GameObject redGeneral;
    public GameObject redColonel;
    public GameObject redMajor;
    public GameObject redCaptain;
    public GameObject redLieutenant;
    public GameObject redSergeant;
    public GameObject redMiner;
    public GameObject redScout;
    public GameObject redSpy;
    public GameObject redBomb;
    public GameObject redFlag;

    // Get blue player pieces
    public GameObject blueMarshall;
    public GameObject blueGeneral;
    public GameObject blueColonel;
    public GameObject blueMajor;
    public GameObject blueCaptain;
    public GameObject blueLieutenant;
    public GameObject blueSergeant;
    public GameObject blueMiner;
    public GameObject blueScout;
    public GameObject blueSpy;
    public GameObject blueBomb;
    public GameObject blueFlag;

    // Set up as 2D array
    private GameObject[,] pieces;

    // Instantiate our players
    private Player red;
    private Player blue;
    public Player currentPlayer;
    public Player otherPlayer;
    private AIPlayer PC;

    // ===
    public Material blueNormal;
    public Material blueHidden;
    public Material redNormal;
    public Material redHidden;

    public Dictionary<string, GameObject> allBlues = new Dictionary<string, GameObject>();
    public Dictionary<string, GameObject> allReds = new Dictionary<string, GameObject>();

    private GameObject gameType;

    // RaycastHit hit = new RaycastHit();
    // Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        if (GameObject.Find("HotSeat"))
        {
            hotseat = true;
        }
        else
        {
            hotseat = false;
        }
    }

    void Start()
    {
        // Gameboard size
        pieces = new GameObject[10, 10];

        // Players
        red = new Player("red", true);
        if (hotseat)
        {
            blue = new Player("blue", false);
        }
        else
        {
            print("AI LEVEL LOADED");
            blue = new Player("blue", false);
            PC = new AIPlayer_UtilityAI();
        }

        // Force menu to be hidden
        EscapeMenu.gameObject.SetActive(false);
        EscapeMenuOff = false;

        currentPlayer = red;
        otherPlayer = blue;

        InitialSetup();

        ChangeColor();
    }

    void Update()
    {
        if (currentPlayer == blue && !hotseat)
        {
            if (Input.GetMouseButton(0))
            {
                // Do nothing
            }
            PC.DoAI();
            NextPlayer();
        }
    }

    void LateUpdate()
    {
        ShowEscapeMenu();
        DisplayCapturedPieces();
    }

    public void InitialSetup()
    {
        // Create a dictionary of all objects for blue and red players
        GameObject[] blues = {
            blueMarshall, blueGeneral, blueColonel, blueCaptain,
            blueLieutenant, blueMajor, blueMiner, blueSpy,
            blueSergeant, blueBomb, blueFlag, blueScout
        };
        GameObject[] reds = {
            redMarshall, redGeneral, redColonel, redCaptain,
            redLieutenant, redMajor, redMiner, redSpy,
            redSergeant, redBomb, redFlag, redScout
        };

        foreach (GameObject obj in blues)
        {
            allBlues.Add(obj.name, obj);
        }

        foreach (GameObject obj in reds)
        {
            allReds.Add(obj.name, obj);
        }

        // Get shuffled blueside
        List<string> blueSide = ShufflePieces(blues);
        List<string> redSide = ShufflePieces(reds);


        // For each object in blueside array, place into scene and populate opposite side of board
        int i = 0;
        for (int j = 0; j < 10; j++)
        {
            for (int k = 9; k > 5; k--)
            {
                // Get string name
                string blueName = blueSide[i];
                if (allBlues.ContainsKey(blueName))
                {
                    GameObject gamePiece = allBlues[blueName];
                    gamePiece.GetComponent<Piece>().internalIdNumber = 200 + i;
                    AddPiece(gamePiece, blue, j, k);
                    i++;
                }
            }
        }

        // !!!!! Allow red player to pick and choose from their array of game objects from a menu... Don't do the work here. !!!!!
        // For each object in blueside array, place into scene and populate opposite side of board

        // ========================================= FOR TESTING PURPOSES ONLY ============================================= //
        i = 0;
        for (int j = 0; j < 10; j++)
        {
            for (int k = 3; k > -1; k--)
            {
                // Get string name
                string redName = redSide[i];
                if (allReds.ContainsKey(redName))
                {
                    GameObject gamePiece = allReds[redName];
                    gamePiece.GetComponent<Piece>().internalIdNumber = 100 + i;
                    AddPiece(gamePiece, red, j, k);
                    i++;
                }
            }
        }
    }

    // =================== Shuffle game pieces

    public List<string> ShufflePieces(GameObject[] allPieces)
    {
        List<string> pieces = new List<string>();
        int i = 0;

        // Shuffle all objects into one array
        foreach (GameObject _piece in allPieces)
        {
            int j = 0;
            int amt = _piece.GetComponent<Piece>().Amount;

            string[] tempList = new string[amt];

            // Use this to add extras into array
            while (j < amt)
            {
                tempList[j] = _piece.name;
                j++;
            }

            // Merging lists
            pieces.AddRange(tempList);
            i++;
        }

        // Shuffle the array here
        for (int k = 0; k < pieces.Count; k++)
        {
            string temp = pieces[k];
            int randomIndex = Random.Range(k, pieces.Count);
            pieces[k] = pieces[randomIndex];
            pieces[randomIndex] = temp;
        }

        // Return the array
        return pieces;
    }


    // =================== Showing allowable moves for piece and board, itself

    public void SelectPieceAtGrid(Vector2Int gridPoint)
    {
        GameObject selectedPiece = pieces[gridPoint.x, gridPoint.y];
        if (selectedPiece)
        {
            board.SelectPiece(selectedPiece);
        }
    }

    public GameObject PieceAtGrid(Vector2Int gridPoint)
    {
        // Grid Pointer (to see potential moves)
        if (gridPoint.x < 0 || gridPoint.x > 9 || gridPoint.y < 0 || gridPoint.y > 9)
        {
            return null;
        }
        return pieces[gridPoint.x, gridPoint.y];
    }

    public List<Vector2Int> MovesForPiece(GameObject pieceObject)
    {
        // Get all legal moves for current game piece
        List<Vector2Int> locations = new List<Vector2Int>();

        if (pieceObject != null)
        {
            // Get all piece components
            Piece piece = pieceObject.GetComponent<Piece>();
            Vector2Int gridPoint = GridForPiece(pieceObject);
            locations = piece.MoveLocations(gridPoint);

            // Filter out offboard locations
            locations.RemoveAll(gp => gp.x < 0 || gp.x > 9 || gp.y < 0 || gp.y > 9);

            locations.RemoveAll(gp => gp == Geometry.GridPoint(2, 4) || gp == Geometry.GridPoint(2, 5) || gp == Geometry.GridPoint(3, 4) || gp == Geometry.GridPoint(3, 5));
            locations.RemoveAll(gp => gp == Geometry.GridPoint(6, 4) || gp == Geometry.GridPoint(6, 5) || gp == Geometry.GridPoint(7, 4) || gp == Geometry.GridPoint(7, 5));

            // filter out locations with friendly piece
            locations.RemoveAll(gp => FriendlyPieceAt(gp));
        }
        return locations;
    }

    public Vector2Int GridForPiece(GameObject piece)
    {
        // Create a grid for the piece for it to move on...
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (pieces[i, j] == piece)
                {
                    return new Vector2Int(i, j);
                }
            }
        }

        return new Vector2Int(-1, -1);
    }


    // =================== Add / Remove pieces from game board

    public void AddPiece(GameObject prefab, Player player, int col, int row)
    {
        GameObject pieceObject = board.AddPiece(prefab, col, row);
        player.pieces.Add(pieceObject);
        pieces[col, row] = pieceObject;
    }

    // Combat takes place here... Flag is an instant win.
    public GameObject CapturePieceAt(GameObject piece, Vector2Int gridPoint)
    {
        // Create local variables so that we can attain variable bits of
        // information like grid point, components to the piece, power level, etc.
        GameObject pieceToCapture = PieceAtGrid(gridPoint);

        // Information pertaining to the pieces themselves
        Piece _pieceToCapComp = pieceToCapture.GetComponent<Piece>();
        Piece _pieceComp = piece.GetComponent<Piece>();
        int _capPwrLevel = _pieceToCapComp.PWR;
        int _pcePwrLevel = _pieceComp.PWR;

        // GameObject to return
        GameObject Returnable;

        // If the piece to get captured is the flag, current player wins!
        if (pieceToCapture.GetComponent<Piece>().type == PieceType.Flag)
        {
            Debug.Log(currentPlayer.name + " wins!");
            Destroy(board.GetComponent<TileSelector>());
            Destroy(board.GetComponent<MoveSelector>());
            
            // Captured!!!!! Like a boss.
            currentPlayer.capturedPieces.Add(pieceToCapture);
            currentPlayer.AllCapturedPieceNames.Add(pieceToCapture.name);
            pieces[gridPoint.x, gridPoint.y] = null;
            board.RemovePiece(pieceToCapture);

            Returnable = piece;
        }
        // Check to see if Marshall has been killed by Spy
        else if (_capPwrLevel == 10 && _pieceComp.CanKillMarshall)
        {
            Debug.LogFormat("You've captured \"{0}\".", _pieceToCapComp.type);
            currentPlayer.capturedPieces.Add(pieceToCapture);
            currentPlayer.AllCapturedPieceNames.Add(pieceToCapture.name);
            pieces[gridPoint.x, gridPoint.y] = null;
            board.RemovePiece(pieceToCapture);

            Returnable = piece;
        }
        // Check to see if Bomb has been killed by Miner
        else if (_capPwrLevel == 11 && _pieceComp.CanKillBomb)
        {
            Debug.LogFormat("You've captured \"{0}\".", _pieceToCapComp.type);
            currentPlayer.capturedPieces.Add(pieceToCapture);
            currentPlayer.AllCapturedPieceNames.Add(pieceToCapture.name);
            pieces[gridPoint.x, gridPoint.y] = null;
            board.RemovePiece(pieceToCapture);

            Returnable = piece;
        }
        // Check to see if current player killed something of lesser value
        else if (_capPwrLevel < _pcePwrLevel)
        {
            Debug.LogFormat("You've captured \"{0}\".", _pieceToCapComp.type);
            currentPlayer.capturedPieces.Add(pieceToCapture);
            currentPlayer.AllCapturedPieceNames.Add(pieceToCapture.name);
            pieces[gridPoint.x, gridPoint.y] = null;
            board.RemovePiece(pieceToCapture);

            Returnable = piece;
        }
        // Check to see if player has been killed by something of greater value
        else if (_capPwrLevel > _pcePwrLevel)
        {
            Debug.LogFormat("Player \"{0}\" has lost \"{1}\" to \"{2}\".", currentPlayer.name, _pieceComp.type, _pieceToCapComp.type);
            otherPlayer.capturedPieces.Add(piece);
            otherPlayer.AllCapturedPieceNames.Add(piece.name);
            // pieces[gridPoint.x, gridPoint.y] = null;  // Piece is nulled here
            board.RemovePiece(piece);

            Returnable = pieceToCapture;
        }
        // Characters were the same number, destroy both.
        else if (_capPwrLevel == _pcePwrLevel)
        {
            Debug.LogFormat("Both players lost their character: \"{0}\".", _pieceComp.type);
            currentPlayer.capturedPieces.Add(pieceToCapture);
            currentPlayer.AllCapturedPieceNames.Add(pieceToCapture.name);
            otherPlayer.capturedPieces.Add(piece);
            otherPlayer.AllCapturedPieceNames.Add(piece.name);

            pieces[gridPoint.x, gridPoint.y] = null;
            board.RemovePiece(pieceToCapture);  // BOOM
            board.RemovePiece(piece);  // BOOM

            Returnable = null;
        }
        else
        {
            Debug.LogError("Unspecified error has occured. Please look into this. Provide error code 999.");
            Returnable = new GameObject();
        }

        return Returnable;
    }

    // =================== Move pieces

    public void Move(GameObject piece, Vector2Int gridPoint)
    {
        Vector2Int startGridPoint = GridForPiece(piece);
        pieces[startGridPoint.x, startGridPoint.y] = null;  // Bug happens here when piece is nulled.
        pieces[gridPoint.x, gridPoint.y] = piece;
        board.MovePiece(piece, gridPoint);
    }

    // =================== Select / Deselect piece

    public void SelectPiece(GameObject piece)
    {
        board.SelectPiece(piece);
        piece.GetComponent<Piece>().text.enabled = true;
    }

    public void DeselectPiece(GameObject piece)
    {
        board.DeselectPiece(piece);
        piece.GetComponent<Piece>().text.enabled = false;
    }

    // =================== Detect pieces

    public bool FriendlyPieceAt(Vector2Int gridPoint)
    {
        GameObject piece = PieceAtGrid(gridPoint);

        if (piece == null)
        {
            return false;
        }

        if (otherPlayer.pieces.Contains(piece))
        {
            return false;
        }

        return true;
    }

    public bool DoesPieceBelongToCurrentPlayer(GameObject piece)
    {
        return currentPlayer.pieces.Contains(piece);
    }

    // =================== Next player

    public void NextPlayer()
    {
        Player tempPlayer = currentPlayer;
        currentPlayer = otherPlayer;
        otherPlayer = tempPlayer;
        ChangeColor();
    }

    // =================== Special functions

    public void ShowEscapeMenu()
    {
        // ResolutionManager
        if (Input.GetKeyDown(KeyCode.Escape) && EscapeMenuOff == true)
        {
            EscapeMenu.gameObject.SetActive(false);
            EscapeMenuOff = false;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && EscapeMenuOff == false)
        {
            EscapeMenu.gameObject.SetActive(true);
            EscapeMenuOff = true;
        }
    }

    public string DisplayCapturedPieces()
    {

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            int _amt = currentPlayer.AllCapturedPieceNames.Count;
            string[] _capturedPieces = new string[_amt];

            Debug.Log(_amt);
            for (int i = 0; i < _amt; i++)
            {
                _capturedPieces[i] = currentPlayer.AllCapturedPieceNames[i];
            }
            string feedback;

            feedback = string.Join("\n\t", _capturedPieces);

            // Debug.LogFormat("Player {0} has captured the following:\n\t{1}", currentPlayer.name, feedback);
            string info = "Player " + currentPlayer.name + " has captured the following:\n\t" + feedback;
            return info;
        }
        else
        {
            return "Player " + currentPlayer.name + " has captured nothing.";
        }
    }

    public void ChangeColor()
    {
        if (currentPlayer == red)
        {
            foreach (GameObject _peace in currentPlayer.pieces)
            {
                if (_peace != null)
                {
                    Transform trans = _peace.GetComponent<Transform>().Find("Character");
                    MeshRenderer mesh = trans.GetComponent<MeshRenderer>();
                    mesh.material = redNormal;
                }
            }

            foreach (GameObject _peace in otherPlayer.pieces)
            {
                if (_peace != null)
                {
                    Transform trans = _peace.GetComponent<Transform>().Find("Character");
                    MeshRenderer mesh = trans.GetComponent<MeshRenderer>();
                    mesh.material = blueHidden;
                }
            }
        }
        else
        {
            foreach (GameObject _peace in currentPlayer.pieces)
            {
                if (_peace != null)
                {
                    Transform trans = _peace.GetComponent<Transform>().Find("Character");
                    MeshRenderer mesh = trans.GetComponent<MeshRenderer>();
                    mesh.material = blueNormal;
                }
            }

            foreach (GameObject _peace in otherPlayer.pieces)
            {
                if (_peace != null)
                {
                    Transform trans = _peace.GetComponent<Transform>().Find("Character");
                    MeshRenderer mesh = trans.GetComponent<MeshRenderer>();
                    mesh.material = redHidden;
                }
            }
        }
    }
}
