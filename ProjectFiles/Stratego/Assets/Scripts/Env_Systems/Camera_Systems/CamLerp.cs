﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamLerp : MonoBehaviour
{
    public float pivotAngle = 90f;
    float pivotVelocity;
    public float speed = .25f;

    public GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetMouseButton(2))
        {
            float theAngle = this.transform.rotation.eulerAngles.y;
            if (theAngle > 180)
            {
                theAngle -= 360f;
            }
            theAngle = Mathf.SmoothDamp(theAngle, (gameManager.currentPlayer.name == "red" ? pivotAngle : -pivotAngle), ref pivotVelocity, speed);


            this.transform.rotation = Quaternion.Euler(new Vector3(0, theAngle, 0));
        }
    }
}
