﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Setting mouse related variables
    public float MouseSensitivity = 1f;
    public float ScrollSensitivity = 2f;

    protected float mouseX, mouseY, mouseScrollWheel;

    protected private float ZoomAmount = 0;
    protected private float MaxToClamp = 15;
    protected private float ROTSpeed = 2;
    
    [Tooltip("'Target' is an empty that is a child of the 'Player' character but a parent of the 'Camera'.")]
    public Transform Target;

    [Tooltip("'Player' is the player character.")]
    public Transform Player;

    [Tooltip("Look at the player or allow the camera to look elsewhere.")]
    public bool LookAtPlayer = true;

    // Start is called before the first frame update
    void Start()
    {
        if (Target.name != "Target" && Target.name != "target")
        {
            Debug.LogError("TARGET must be assigned properly, please name it 'Target' or 'target'. Invalidating.");
            Target = null;
            Player = null;
        } else if (Target.name == null || Player.name == null)
        {
            Debug.LogError("Target and Player must be assigned. Invalidating.");
            Target = null;
            Player = null;
        }

        // Make cursor invisible and lock it to the window.
        // Cursor.visible = false;
        // Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(2))
        {
            // Testing to see if we have a null, otherwise we will not continue.
            if (Target != null && Player != null)
            {
                // Rotation function
                CameraRotation();
            }
        }
        // Zoom function
        CameraZoom();

        // Always look at the player
        if (LookAtPlayer)
        {
            transform.LookAt(Player.position);
        }
    }

    void CameraRotation()
    {
        // Get the mouse input
        mouseX += Input.GetAxis("Mouse X") * MouseSensitivity;
        // mouseY -= Input.GetAxis("Mouse Y") * MouseSensitivity;

        // Clamp the mouse to a specific movement
        // mouseY = Mathf.Clamp(mouseY, 5, 80);

        // Properly rotate the camera around the player
        Target.rotation = Quaternion.Euler(0, -mouseX, 0);
    }

    void CameraZoom()
    {
        /*
        // Get the mouse scrollwheel input
        mouseScrollWheel = Input.GetAxis("Mouse ScrollWheel") * ScrollSensitivity;

        // Clamp the zoom amount so that we will never go below or above the maximum
        ZoomAmount += mouseScrollWheel;
        ZoomAmount = Mathf.Clamp(ZoomAmount, -MaxToClamp, MaxToClamp);

        // translate or dolly or "zoom" towards the player
        float translate = Mathf.Min(Mathf.Abs(mouseScrollWheel), MaxToClamp - Mathf.Abs(ZoomAmount));
        float ZedZoomDistance = translate * ROTSpeed * Mathf.Sign(mouseScrollWheel);

        /// TODO: FIX ZOOM
        if (transform.position.z < -1)
        {
            gameObject.transform.Translate(0, 0, ZedZoomDistance);
        }
        else
        {
            gameObject.transform.Translate(0, 0, -1);
        }
        */

        // This works but is slow
        ZoomAmount += Input.GetAxis("Mouse ScrollWheel") * ScrollSensitivity;
        ZoomAmount = Mathf.Clamp(ZoomAmount, -MaxToClamp, MaxToClamp);
        float translate = Mathf.Min(Mathf.Abs(Input.GetAxis("Mouse ScrollWheel")), MaxToClamp - Mathf.Abs(ZoomAmount));
        gameObject.transform.Translate(0, 0, translate * ROTSpeed * Mathf.Sign(Input.GetAxis("Mouse ScrollWheel") * ScrollSensitivity));
        
    }
}
