﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundtrackHandler : MonoBehaviour
{
    public AudioClip[] clips;
    private AudioSource songPlayer;

    // Start is called before the first frame update
    void Start()
    {
        songPlayer = GameObject.Find("SoundtrackManager").AddComponent<AudioSource>();
        songPlayer.name = "SoundtrackPlayer";
    }

    // Update is called once per frame
    void Update()
    {
        if (songPlayer != null)
        {
            if (!songPlayer.isPlaying)
            {
                int rand = Random.Range(0, clips.Length);
                songPlayer.clip = clips[rand];
                songPlayer.loop = false;
                songPlayer.Play(0);
            }
        }
    }
}
