﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ResolutionManager : MonoBehaviour
{
    public int[] resolution;
    public bool fullscreen;

    public void SetFullscreen(bool newFullscreen)
    {
        // Do we want full screen or not?
        fullscreen = newFullscreen;
    }

    public void ResToChange(int attainedInt)
    {
        // Get the resolutions so we can change later
        int[] res = new int[2];
        switch (attainedInt)
        {
            case 0:
                res[0] = 800;
                res[1] = 600;
                break;
            case 1:
                res[0] = 1200;
                res[1] = 720;
                break;
            case 2:
                res[0] = 1600;
                res[1] = 900;
                break;
            case 3:
                res[0] = 1920;
                res[1] = 1080;
                break;
            default:
                res[0] = 1200;
                res[1] = 720;
                break;
        }
        resolution = res;
    }

    public void SetRes()
    {
        // Change screen resolution
        Screen.SetResolution(resolution[0], resolution[1], fullscreen);
    }

    public void MainMenu()
    {
        // Quit the application
        SceneManager.LoadScene("Main");
    }
}
