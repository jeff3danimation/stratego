﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuHandler : MonoBehaviour
{
    public Button singlePlayer;
    public Button hotSeat;
    public Button multiPlayer;
    public Button Quit;
    private GameObject gameType;

    private void Awake()
    {
        gameType = new GameObject();
        Spawn();
    }

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameType);
        singlePlayer.onClick.AddListener(LoadSinglePlayer);
        hotSeat.onClick.AddListener(LoadHotSeat);
        Quit.onClick.AddListener(QuitGame);
    }

    void Spawn()
    {
        Instantiate(gameType, transform.position, Quaternion.identity);
    }

    void LoadSinglePlayer()
    {
        gameType.name = "SinglePlayer";
        SceneManager.LoadScene("Game");
    }

    void LoadHotSeat()
    {
        gameType.name = "HotSeat";
        SceneManager.LoadScene("Game");
    }

    void QuitGame()
    {
        Debug.Log("Quitting Stratego... :(");
        Application.Quit();
    }
}
