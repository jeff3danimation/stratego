﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterials : MonoBehaviour
{
    // Get materials
    public Material blueNormal;
    public Material blueHidden;
    public Material redNormal;
    public Material redHidden;

    private bool playerOneTurn;

    public GameObject[] PlausibleRedPieces;
    public GameObject[] PlausibleBluePieces;

    public GameManager gm;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ChangeColor()
    {
        if (!playerOneTurn)
        {
            playerOneTurn = !playerOneTurn;
            foreach (GameObject redobj in PlausibleRedPieces)
            {
                Transform redtrans = redobj.GetComponent<Transform>().Find("Character");
                MeshRenderer redmesh = redtrans.GetComponent<MeshRenderer>();
                redmesh.material = redHidden;
            }
            foreach (GameObject blueobj in PlausibleBluePieces)
            {
                Transform bluetrans = blueobj.GetComponent<Transform>().Find("Character");
                MeshRenderer bluemesh = bluetrans.GetComponent<MeshRenderer>();
                bluemesh.material = blueNormal;
            }
        }
        else
        {
            playerOneTurn = !playerOneTurn;
            foreach (GameObject blueobj in PlausibleBluePieces)
            {
                Transform bluetrans = blueobj.GetComponent<Transform>().Find("Character");
                MeshRenderer bluemesh = bluetrans.GetComponent<MeshRenderer>();
                bluemesh.material = blueHidden;
            }

            foreach (GameObject redobj in PlausibleRedPieces)
            {
                Transform redtrans = redobj.GetComponent<Transform>().Find("Character");
                MeshRenderer redmesh = redtrans.GetComponent<MeshRenderer>();
                redmesh.material = redNormal;
            }
        }
    }
}
