﻿using System.Collections.Generic;
using UnityEngine;

public class Scout : Piece
{
    public override List<Vector2Int> MoveLocations(Vector2Int gridPoint)
    {
        List<Vector2Int> locations = new List<Vector2Int>();

        foreach (Vector2Int dir in StandardDirections)
        {
            for (int i = 1; i < 10; i++)
            {
                Vector2Int nextGridPoint = new Vector2Int(gridPoint.x + i * dir.x, gridPoint.y + i * dir.y);
                locations.Add(nextGridPoint);
                if (GameManager.instance.PieceAtGrid(nextGridPoint))
                {
                    break;
                }
                if (nextGridPoint == Geometry.GridPoint(2, 4) || nextGridPoint == Geometry.GridPoint(2, 5) || nextGridPoint == Geometry.GridPoint(3, 4) || nextGridPoint == Geometry.GridPoint(3, 5) || nextGridPoint == Geometry.GridPoint(6, 4) || nextGridPoint == Geometry.GridPoint(6, 5) || nextGridPoint == Geometry.GridPoint(7, 4) || nextGridPoint == Geometry.GridPoint(7, 5))
                {
                    break;
                }
            }
        }
        return locations;
    }
}
