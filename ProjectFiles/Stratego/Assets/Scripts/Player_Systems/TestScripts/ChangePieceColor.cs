﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePieceColor : MonoBehaviour
{
    public Material material1;
    public Material material2;
    private bool test;

    // Start is called before the first frame update
    void Start()
    {
        tester();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            tester();
        }
    }

    void tester()
    {
        Transform mesh = this.GetComponent<Transform>().Find("Character");
        MeshRenderer rend = mesh.GetComponent<MeshRenderer>();
        if (!test)
        {
            test = !test;
            rend.material = material1;
        }
        else
        {
            test = !test;
            rend.material = material2;
        }
    }
}
