﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    public Material material1;
    public Material material2;
    private bool test;

    // Start is called before the first frame update
    void Start()
    {
        tester();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            tester();
        }
    }

    void tester()
    {
        MeshRenderer mesh = this.GetComponent<MeshRenderer>();
        if (!test)
        {
            test = !test;
            mesh.material = material1;
        }
        else
        {
            test = !test;
            mesh.material = material2;
        }
    }
}
