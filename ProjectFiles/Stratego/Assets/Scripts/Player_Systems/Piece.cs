﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum PieceType { Marshall, General, Colonel, Major, Captain, Lieutenant, Sergeant, Miner, Scout, Spy, Bomb, Flag };

public abstract class Piece : MonoBehaviour, IInventoryItem
{
    public TMP_Text text;
    public int internalIdNumber;

    public string Name
    {
        get
        {
            return name;
        }
    }

    public Sprite _Image = null;

    public Sprite Image
    {
        get
        {
            return _Image;
        }
    }

    void Start()
    {
        text = gameObject.GetComponentInChildren<TextMeshPro>();

        if (text != null)
        {
            string power = PWR.ToString();
            string typer = type.ToString();
            string moves;
            if (type == PieceType.Scout)
            {
                moves = "Up to 9";
            }
            else
            {
                moves = "1";
            }
            string a = ("Name: " + typer + "\nPower: " + power + "\nMovement: " + moves);

            // Set the text, enable backface culling and disable the visibility.
            text.SetText(a);
            text.enableCulling = true;
            text.enabled = false;
        }
    }

    public void OnPickup()
    {
        gameObject.SetActive(false);
    }

    public void OnDrop()
    {
        RaycastHit hit = new RaycastHit();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000))
        {
            gameObject.SetActive(true);
            gameObject.transform.position = hit.point;
        }
    }

    void Update()
    {
        RaycastHit hit = new RaycastHit();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000))
        {
            // TODO: Make this only apply to one piece at a time --- Currently applies to ALL pieces
            /*
            if (GameManager.instance.DoesPieceBelongToCurrentPlayer(gameObject))
            {
                text.enabled = true;
            }
            */
        }
    }

    public PieceType type;

    [Tooltip("Kill Marshall override.")]
    public bool CanKillMarshall = false;

    [Tooltip("Kill Bomb override.")]
    public bool CanKillBomb = false;

    [Tooltip("The Power of the piece. Can it kill things it attacks?")]
    public int PWR = 0;

    [Tooltip("How important this piece is.")]
    public int Importance = 1;

    [Tooltip("Amount of pieces for this character.")]
    public int Amount = 1;

    protected Vector2Int[] StandardDirections = { new Vector2Int(0, 1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0)};

    public abstract List<Vector2Int> MoveLocations(Vector2Int gridPoint);
}
