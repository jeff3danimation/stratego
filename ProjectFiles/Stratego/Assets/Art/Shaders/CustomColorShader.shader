﻿Shader "Custom/Stratego Diffuse"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_SpecColor("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess("Shininess", Range(0.03, 1)) = 0.078125
		_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
	}
	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}
		LOD 400

		CGPROGRAM
		#pragma surface surf BlinnPhong

		sampler2D _MainTex;
		fixed4 _Color;
		half _Shininess;

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o)
		{
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
			/*
			if (tex.r < 0.01 && tex.g < 0.01 && tex.b < 0.01)
			{
				o.Albedo = _Color.rgb;
			}
			else
			{
				o.Albedo = tex.rgb;
			}
			*/

			o.Albedo = (tex.rgb * tex.a) + ((1 - tex.a) * _Color.rgb);
			o.Gloss = tex.a;
			o.Alpha = tex.a * _Color.a;
			o.Specular = _Shininess;
		}
		ENDCG
	}
	FallBack "Specular"
}
